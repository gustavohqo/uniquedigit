package br.com.challenge.uniquedigit.service;

import br.com.challenge.uniquedigit.exception.ConcatFactorOutOfRangeException;
import br.com.challenge.uniquedigit.exception.NotInRangeNumberException;
import br.com.challenge.uniquedigit.exception.ParseNumberException;
import br.com.challenge.uniquedigit.infra.cache.UniqueDigitCalculationCache;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.Spy;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UniqueDigitShould {

    UniqueDigitCalculationCache cache;

    @Spy
    UniqueDigit uniqueDigit;

    @BeforeEach
    public void SetUp() {
        cache = new UniqueDigitCalculationCache(1);
        uniqueDigit = Mockito.spy(new UniqueDigit(cache));
    }

    @Test
    public void return_An_Integer_When_Called_With_Correct_Params() throws RuntimeException {
        Assertions.assertNotNull(uniqueDigit.getUniqueFrom("1"));
    }

    @Test
    public void return_Correct_Value_When_Passing_A_String_Representation_Of_A_Positive_Integer_Number() throws RuntimeException {
        Integer expected = 1;

        String number1 = Integer.toString(1);
        String number10 = Integer.toString(10);
        String number100 = Integer.toString(100);

        Integer number1UniqueDigit = uniqueDigit.getUniqueFrom(number1);
        Integer number10UniqueDigit = uniqueDigit.getUniqueFrom(number10);
        Integer number100UniqueDigit = uniqueDigit.getUniqueFrom(number100);

        Assertions.assertEquals(expected, number1UniqueDigit);
        Assertions.assertEquals(expected, number10UniqueDigit);
        Assertions.assertEquals(expected, number100UniqueDigit);

        expected = 3;
        String number12 = Integer.toString(12);
        Integer number12UniqueDigit = uniqueDigit.getUniqueFrom(number12);
        Assertions.assertEquals(expected, number12UniqueDigit);

        expected = 120;
        Integer numberTooLargeUniqueDigit = uniqueDigit.getUniqueFrom("222222222222222222222222222222222222222222222222222222222222");
        Assertions.assertEquals(expected, numberTooLargeUniqueDigit);
    }

    @Test
    public void throw_A_Parse_Number_Exception_When_Parameter_Is_Not_A_Representation_Of_A_Integer_Number() {
        String notANumber1 = "It is not A number";
        Assertions.assertThrows(ParseNumberException.class, () -> uniqueDigit.getUniqueFrom(notANumber1));

        String notANumber2 = "123ABC";
        Assertions.assertThrows(ParseNumberException.class, () -> uniqueDigit.getUniqueFrom(notANumber2));
    }

    @Test
    public void throw_A_Not_In_Range_Number_Exception_When_The_Number_Represented_Is_Zero_Or_Negative() {
        String notInTheRange1 = "0";
        Assertions.assertThrows(NotInRangeNumberException.class, () -> uniqueDigit.getUniqueFrom(notInTheRange1));

        String notInTheRange2 = "-55";
        Assertions.assertThrows(NotInRangeNumberException.class, () -> uniqueDigit.getUniqueFrom(notInTheRange2));
    }

    @Test
    public void return_Correct_Value_When_Passing_The_Concat_Parameter() {
        int concatFactor = 2;
        String numberRepresentation = "10";

        int expected = 2;
        Assertions.assertEquals(expected, uniqueDigit.getUniqueFrom(numberRepresentation, concatFactor));

        expected = 240;
        numberRepresentation = "222222222222222222222222222222222222222222222222222222222222";
        Integer numberTooLargeUniqueDigit = uniqueDigit.getUniqueFrom(numberRepresentation, concatFactor);
        Assertions.assertEquals(expected, numberTooLargeUniqueDigit);

        concatFactor = 10;
        expected = 1200;
        numberTooLargeUniqueDigit = uniqueDigit.getUniqueFrom(numberRepresentation, concatFactor);
        Assertions.assertEquals(expected, numberTooLargeUniqueDigit);
    }

    @Test
    public void throw_Factor_Out_Of_Range_Exception_When_Concat_Factor_IsGreat_Then_10_Power_5_Or_Less_Then_One() {
        int concatFactor1 = 100001;
        String numberRepresentation = "1123";

        Assertions.assertThrows(ConcatFactorOutOfRangeException.class, () -> uniqueDigit.getUniqueFrom(numberRepresentation, concatFactor1));

        int concatFactor2 = 0;

        Assertions.assertThrows(ConcatFactorOutOfRangeException.class, () -> uniqueDigit.getUniqueFrom(numberRepresentation, concatFactor2));
    }

    @Test
    public void call_the_calculation_method_only_once_when_try_to_calculate_the_same_unique_digit_twice() {
        int concatFactor = 2;
        String numberRepresentation = "10";

        int expected = 2;
        Assertions.assertEquals(expected, uniqueDigit.getUniqueFrom(numberRepresentation, concatFactor));
        uniqueDigit.getUniqueFrom(numberRepresentation, concatFactor);

        Mockito.verify(uniqueDigit, Mockito.times(1)).calculateUniqueDigit("1010");
    }

    @Test
    public void call_the_calculation_method_more_than_once_when_try_to_calculate_different_unique_digit() {
        int concatFactor = 2;
        String numberRepresentation1 = "10";
        String numberRepresentation2 = "11";

        uniqueDigit.getUniqueFrom(numberRepresentation1, concatFactor);
        uniqueDigit.getUniqueFrom(numberRepresentation2, concatFactor);

        ArgumentCaptor<String> keyCaptor = ArgumentCaptor.forClass(String.class);

        Mockito.verify(uniqueDigit, Mockito.times(2)).calculateUniqueDigit(keyCaptor.capture());
    }
}