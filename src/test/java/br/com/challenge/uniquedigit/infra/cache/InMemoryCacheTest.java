package br.com.challenge.uniquedigit.infra.cache;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InMemoryCacheShould {

    private Cache<Integer, String> cache;

    @BeforeEach
    void setUp() {
        this.cache = new InMemoryCache<>(3);
    }

    @Test
    void add_and_retrieve() {
        String cacheObject = "primeiro";
        Integer key = 1;
        cache.add(key, cacheObject);

        assertEquals(cacheObject, cache.get(key));
    }

    @Test
    void add_no_more_then_three_entries() {
        String cacheEntry1 = "primeiro";
        String cacheEntry2 = "segundo";
        String cacheEntry3 = "terceiro";
        String cacheEntry4 = "quarto";


        Integer key1 = 1;
        Integer key2 = 2;
        Integer key3 = 3;
        Integer key4 = 4;

        cache.add(key1, cacheEntry1);
        cache.add(key2, cacheEntry2);
        cache.add(key3, cacheEntry3);

        assertEquals(3, cache.size());

        cache.add(key4, cacheEntry4);

        assertEquals(3, cache.size());
    }

    @Test
    void remove_the_first_entry_added_when_add_the_fourth() {
        String cacheEntry1 = "primeiro";
        String cacheEntry2 = "segundo";
        String cacheEntry3 = "terceiro";
        String cacheEntry4 = "quarto";


        Integer key1 = 1;
        Integer key2 = 2;
        Integer key3 = 3;
        Integer key4 = 4;

        cache.add(key1, cacheEntry1);
        cache.add(key2, cacheEntry2);
        cache.add(key3, cacheEntry3);
        cache.add(key4, cacheEntry4);

        assertNull(cache.get(key1));
        assertEquals(cacheEntry2, cache.get(key2));
    }

    @Test
    void update_an_entry_accessed_to_mru_position() {
        String cacheEntry1 = "primeiro";
        String cacheEntry2 = "segundo";
        String cacheEntry3 = "terceiro";
        String cacheEntry4 = "quarto";
        String cacheEntry5 = "quinto";

        Integer key1 = 1;
        Integer key2 = 2;
        Integer key3 = 3;
        Integer key4 = 4;
        Integer key5 = 5;

        cache.add(key1, cacheEntry1);
        cache.add(key2, cacheEntry2);
        cache.add(key3, cacheEntry3);
        cache.add(key4, cacheEntry4);

        assertNull(cache.get(key1));
        assertEquals(cacheEntry2, cache.get(key2));

        cache.add(key5, cacheEntry5);

        assertNull(cache.get(key3));

    }
}