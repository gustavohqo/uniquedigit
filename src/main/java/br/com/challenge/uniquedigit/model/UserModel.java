package br.com.challenge.uniquedigit.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "TBL_USER")
public class UserModel {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;
    @Column
    private String email;
    @OneToMany
    private List<UniqueDigitCalculation> calculations;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<UniqueDigitCalculation> getCalculations() {
        return calculations;
    }

    public void setCalculations(List<UniqueDigitCalculation> calculations) {
        this.calculations = calculations;
    }
}
