package br.com.challenge.uniquedigit.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_unique_digit_calculations")
public class UniqueDigitCalculation {

    @Id
    @GeneratedValue
    private long id;

    private String numberRepresentation;
    private int concatFactor;
    private int uniqueDigitResult;

    public UniqueDigitCalculation() {
    }

    public UniqueDigitCalculation(String numberRepresentation, int concatFactor, int uniqueDigitResult) {
        this.numberRepresentation = numberRepresentation;
        this.concatFactor = concatFactor;
        this.uniqueDigitResult = uniqueDigitResult;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumberRepresentation() {
        return numberRepresentation;
    }

    public void setNumberRepresentation(String numberRepresentation) {
        this.numberRepresentation = numberRepresentation;
    }

    public int getConcatFactor() {
        return concatFactor;
    }

    public void setConcatFactor(int concatFactor) {
        this.concatFactor = concatFactor;
    }

    public int getUniqueDigitResult() {
        return uniqueDigitResult;
    }

    public void setUniqueDigitResult(int uniqueDigitResult) {
        this.uniqueDigitResult = uniqueDigitResult;
    }
}
