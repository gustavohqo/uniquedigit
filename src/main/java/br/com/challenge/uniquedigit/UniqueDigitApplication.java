package br.com.challenge.uniquedigit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniqueDigitApplication {

    public static void main(String[] args) {
        SpringApplication.run(UniqueDigitApplication.class, args);
    }

}
