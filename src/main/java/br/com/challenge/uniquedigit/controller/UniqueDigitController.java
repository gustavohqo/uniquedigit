package br.com.challenge.uniquedigit.controller;

import br.com.challenge.uniquedigit.model.UniqueDigitCalculation;
import br.com.challenge.uniquedigit.service.CalculationService;
import br.com.challenge.uniquedigit.service.UniqueDigit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UniqueDigitController {
    @Autowired
    private CalculationService calculationService;

    @GetMapping
    public UniqueDigitCalculation getUniqueDigit(@RequestParam String number,
                                                 @RequestParam Integer concatFactor,
                                                 @RequestParam(required = false) Long userId) {
        return calculationService.calculateUniqueDigit(number, concatFactor, userId);
    }
}
