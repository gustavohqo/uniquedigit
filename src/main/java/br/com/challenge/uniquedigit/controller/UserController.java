package br.com.challenge.uniquedigit.controller;

import br.com.challenge.uniquedigit.model.UniqueDigitCalculation;
import br.com.challenge.uniquedigit.model.UserModel;
import br.com.challenge.uniquedigit.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping(value = "/{userId}")
    public UserModel getById(@PathVariable Long userId) {
        return userService.getUserById(userId);
    }

    @GetMapping(value = "/{email}")
    public UserModel getByEmail(@PathVariable String email) {
        return userService.getUserByEmail(email);
    }

    @PostMapping
    public UserModel save(@RequestBody UserModel userModel) {
        return userService.addUser(userModel.getName(), userModel.getEmail());
    }

    @PutMapping
    public  UserModel edit(@RequestBody UserModel userModel) {
        return  userService.saveUser(userModel);
    }

    @DeleteMapping
    public void delete(@RequestBody UserModel userModel) {
        userService.deleteUser(userModel);
    }

    @GetMapping(value = "/{userId}/calculations")
    public List<UniqueDigitCalculation> getCalculationsFromUser(@PathVariable Long userId) {
        UserModel user = userService.getUserById(userId);

        return user.getCalculations();
    }
}
