package br.com.challenge.uniquedigit.infra.cache;

import br.com.challenge.uniquedigit.model.UniqueDigitCalculation;
import org.springframework.web.context.annotation.ApplicationScope;

@ApplicationScope
public class UniqueDigitCalculationCache extends InMemoryCache<String, UniqueDigitCalculation> {
    public UniqueDigitCalculationCache(int maximumNumberOfItens) {
        super(maximumNumberOfItens);
    }

    public UniqueDigitCalculation get(String numberString, int concatFactor) {
        return super.get(numberString + concatFactor);
    }

    public void put(UniqueDigitCalculation calculation) {
        super.add(calculation.getNumberRepresentation() + calculation.getConcatFactor(), calculation);
    }
}
