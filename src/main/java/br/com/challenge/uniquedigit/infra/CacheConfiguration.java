package br.com.challenge.uniquedigit.infra;

import br.com.challenge.uniquedigit.infra.cache.UniqueDigitCalculationCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CacheConfiguration {

    @Bean
    public UniqueDigitCalculationCache configure() {
        return new UniqueDigitCalculationCache(10);
    }
}
