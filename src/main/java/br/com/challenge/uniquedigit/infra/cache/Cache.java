package br.com.challenge.uniquedigit.infra.cache;

public interface Cache<K, T> {

    void add(K key, T object);

    T get(K key);

    void remove(K key);

    int size();

    void clear();
}
