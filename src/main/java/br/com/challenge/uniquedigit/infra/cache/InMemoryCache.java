package br.com.challenge.uniquedigit.infra.cache;


import org.apache.commons.collections4.map.LRUMap;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class InMemoryCache<K, T> implements Cache<K, T> {

    private LRUMap<K, T> cacheMap;

    public InMemoryCache(int maximumNumberOfItens) {
        cacheMap = new LRUMap<K, T>(maximumNumberOfItens);
    }

    @Override
    public void add(K key, T object) {
        synchronized (cacheMap) {
            cacheMap.put(key, object);
        }
    }

    @Override
    public T get(K key) {
        synchronized (cacheMap) {
            return cacheMap.get(key, true);
        }
    }

    @Override
    public void remove(K key) {
        synchronized (cacheMap) {
            cacheMap.remove(key);
        }
    }

    @Override
    public int size() {
        synchronized (cacheMap) {
            return cacheMap.size();
        }
    }

    @Override
    public void clear() {
        throw new NotImplementedException();
    }
}
