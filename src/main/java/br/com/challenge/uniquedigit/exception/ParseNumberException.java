package br.com.challenge.uniquedigit.exception;

public class ParseNumberException extends NotAcceptableException {
    public ParseNumberException() {
        super("To calculate Unique Digit must not have any character besides digits.");
    }
}
