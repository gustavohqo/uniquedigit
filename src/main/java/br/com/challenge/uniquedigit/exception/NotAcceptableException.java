package br.com.challenge.uniquedigit.exception;

import org.springframework.http.HttpStatus;

public class NotAcceptableException extends RuntimeException {
    private HttpStatus code = HttpStatus.NOT_ACCEPTABLE;

    public NotAcceptableException(String message) {
        super(message);
    }

    public HttpStatus getCode() {
        return code;
    }

    public void setCode(HttpStatus code) {
        this.code = code;
    }
}
