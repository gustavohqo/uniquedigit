package br.com.challenge.uniquedigit.exception;

public class NotInRangeNumberException extends NotAcceptableException {

    public NotInRangeNumberException() {
        super("The number informed must be greater than 0");
    }
}
