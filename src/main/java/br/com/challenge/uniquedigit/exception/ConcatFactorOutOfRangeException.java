package br.com.challenge.uniquedigit.exception;

public class ConcatFactorOutOfRangeException extends NotAcceptableException {

    public ConcatFactorOutOfRangeException() {
        super("The concatenation factor must be greater than 0 and lesser than 100000");
    }
}
