package br.com.challenge.uniquedigit.service;

import br.com.challenge.uniquedigit.model.UniqueDigitCalculation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class CalculationService {
    @Autowired
    private UniqueDigit uniqueDigit;

    @Autowired
    private UserService userService;

    public UniqueDigitCalculation calculateUniqueDigit(String numberRepresentation,
                                                       Integer concatFactor,
                                                       Long userId) {

        Integer result = uniqueDigit.getUniqueFrom(numberRepresentation, concatFactor);
        UniqueDigitCalculation calculation = new UniqueDigitCalculation(numberRepresentation, concatFactor, result);

        if(!Objects.isNull(userId)) {
            userService.addCalculationToUser(userId, calculation);
        }

        return calculation;
    }
}
