package br.com.challenge.uniquedigit.service;

import br.com.challenge.uniquedigit.exception.ConcatFactorOutOfRangeException;
import br.com.challenge.uniquedigit.exception.NotInRangeNumberException;
import br.com.challenge.uniquedigit.exception.ParseNumberException;
import br.com.challenge.uniquedigit.infra.cache.UniqueDigitCalculationCache;
import br.com.challenge.uniquedigit.model.UniqueDigitCalculation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.Objects;


@Component
public class UniqueDigit {
    private UniqueDigitCalculationCache cache;

    public UniqueDigit(UniqueDigitCalculationCache cache) {
        this.cache = cache;
    }

    public Integer getUniqueFrom(String numberString, int concatFactor) {
        this.validateNumberRepresentation(numberString);
        this.validadeConcatFactor(concatFactor);

        UniqueDigitCalculation calculation = cache.get(numberString, concatFactor);
        if (Objects.isNull(calculation)) {
            String newNumberString = numberString;

            if (numberString.length() == 1 && concatFactor == 1) return Integer.valueOf(numberString);
            if (concatFactor > 1) newNumberString = applyConcatFactor(numberString, concatFactor);

            calculation = new UniqueDigitCalculation(numberString, concatFactor, calculateUniqueDigit(newNumberString));
            this.cache.put(calculation);
        }

        return calculation.getUniqueDigitResult();
    }

    public Integer getUniqueFrom(String numberString) {
        return this.getUniqueFrom(numberString, 1);
    }

    private String applyConcatFactor(String numberString, int k) {
        return StringUtils.repeat(numberString, k);
    }

    public int calculateUniqueDigit(String numberString) {
        int stringLength = numberString.length();
        int sum = 0;
        for (int i = 0; i < stringLength; i++) {
            String stringOfChar = Character.toString(numberString.charAt(i));
            int charValue = Integer.parseInt(stringOfChar);
            sum += charValue;
        }
        return sum;
    }

    private void validateNumberRepresentation(String numberString) {
        try {
            BigInteger number = new BigInteger(numberString);

            if (number.compareTo(new BigInteger("0")) <= 0) throw new NotInRangeNumberException();

        } catch (NumberFormatException e) {
            throw new ParseNumberException();
        }
    }

    private void validadeConcatFactor(int concatFactor) {
        int MIN_VALUE = 1;
        int MAX_VALUE = 100000;

        if (concatFactor < MIN_VALUE || concatFactor > MAX_VALUE) throw new ConcatFactorOutOfRangeException();
    }
}
