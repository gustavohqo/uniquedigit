package br.com.challenge.uniquedigit.service;

import br.com.challenge.uniquedigit.exception.NotFoundException;
import br.com.challenge.uniquedigit.model.UniqueDigitCalculation;
import br.com.challenge.uniquedigit.model.UserModel;
import br.com.challenge.uniquedigit.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public UserModel getUserByEmail(String email) {
        Optional<UserModel>  user = userRepository.findByEmail(email);

        if(!user.isPresent()) {
            throw new NotFoundException(
                    new StringBuilder().append("User with e-mail: ")
                    .append(email)
                    .append(" was not found.")
                    .toString());
        }

        return user.get();
    }

    public UserModel getUserById(Long id) {
        Optional<UserModel>  user = userRepository.findById(id);

        if(!user.isPresent()) {
            throw new NotFoundException(
                    new StringBuilder().append("User with id: ")
                    .append(id)
                    .append(" was not found.")
                    .toString());
        }

        return user.get();
    }

    public UserModel addUser(String name, String email) {
        UserModel newUser = new UserModel();

        newUser.setName(name);
        newUser.setEmail(email);

        return userRepository.save(newUser);
    }

    public UserModel saveUser(UserModel userModel) {
        return userRepository.save(userModel);
    }

    public void deleteUser(UserModel userModel) {
        userRepository.delete(userModel);
    }

    public void addCalculationToUser(Long userId, UniqueDigitCalculation calculation) {
        UserModel user = this.getUserById(userId);
        user.getCalculations().add(calculation);
        saveUser(user);
    }
}
